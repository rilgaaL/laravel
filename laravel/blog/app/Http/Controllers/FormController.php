<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form(){
        return view('form');
    }

    public function welcome(Request $request){
        return "welcome_h1";
    }
    public function welcome_post(Request $request){
        //dd($request->all());
        $nama1 = $request["nama1"];
        $nama2 = $request["nama2"];
        return view('welcome', compact('nama1', 'nama2'));
    }
}
