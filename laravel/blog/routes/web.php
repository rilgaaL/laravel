<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/form', 'FormController@form' );

Route::get('/welcome', 'FormController@welcome' );
Route::post('/welcome', 'FormController@welcome_post' );

//Route::get('/welcome/{nama}', function ($nama) {
//    return "Welcome $nama";
//});