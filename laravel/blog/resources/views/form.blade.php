<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title></title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <form action="/welcome" method="POST">
            @csrf
            <label for="first_name">First name:</label> <br><br>
            <input type="text" name="nama1"> <br><br>
            <label for="last_name">Last name:</label> <br><br>
            <input type="text" name="nama2"> <br><br>
            <label for="gender">Gender:</label> <br><br>
            <input type="radio" name="gender" id="male">Male <br>
            <input type="radio" name="gender" id="female">Female <br>
            <input type="radio" name="gender" id="other">Other <br><br>
            <label for="nationality">Nationality:</label> <br><br>
            <select name="nationality">
                <option value="indonesian">indonesian</option>
                <option value="malaysian">malaysian</option>
                <option value="singapura">singapura</option>
            </select> <br><br>
            <label for="language">Language Spoken:</label> <br><br>
            <input type="checkbox" name="language" id="bahasa_indonesia">Bahasa Indonesia <br>
            <input type="checkbox" name="language" id="english">English <br>
            <input type="checkbox" name="language" id="other">Other <br><br>
            <label>Bio:</label> <br><br>
            <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>